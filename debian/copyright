Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xnote++
Upstream-Contact: xnote-users@googlegroups.com
Source: https://github.com/xnotepp/xnote

Files:     *
Copyright: 2009-2024 Lorenz Froihofer, opto
License:   LGPL-2.1

Files:     chrome/content/xnote-window.js
Copyright: Hugo Smadja, Lorenz Froihofer
License:   LGPL-2.1

Files:     chrome/content/xnote-window.xhtml
Copyright: Hugo Smadja, Lorenz Froihofer
License:   LGPL-2.1

Files:     chrome/content/xnote-overlay.js
Copyright: Hugo Smadja, Pierre-Andre Galmes, Lorenz Froihofer
License:   LGPL-2.1

Files:     extlib/l10n.js
Copyright: 2016-2018 YUKI "Piro" Hiroshi
License:   MIT

Files:     debian/*
Copyright: 2021-2024 Mechtilde Stehmann <mechtilde@debian.org>
License: LGPL-2.1

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 .
Comment:
 On Debian systems, the complete text of the GNU Lesser General 
 Public License Version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

License:   MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation the .
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
