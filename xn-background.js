/*
 * Documentation:
 * https://github.com/thundernest/addon-developer-support/wiki/Using-the-WindowListener-API-to-convert-a-Legacy-Overlay-WebExtension-into-a-MailExtension-for-Thunderbird-78
 */

//TODO
/*
x   note does not close
displaying a new note by click triggers unload listener (ca. 6 times)

upgrade, new pref
*/

'use strict';

const debug = false;//"@@@DEBUGFLAG@@@";

var lastTab = 0, lastWindow = 0;
var openMsgs = [];

var preferenceCache;

var donTimer;

var xnoteKeys = [];

var xnote = {};
xnote.text = "";
xnote.date = "";
xnote.inMsgDisplay = false;
xnote.window = null;
xnote.currentDisplayedMessage = -1;

// Register the chrome url, so any code running after this can access them.
messenger.WindowListener.registerChromeUrl([
  ["content", "xnote", "chrome/content/"],
  ["resource", "xnote", "chrome/"],

  ["locale", "xnote", "en-US", "chrome/locale/en-US/"],
  ["locale", "xnote", "de", "chrome/locale/de/"],
  ["locale", "xnote", "fr-FR", "chrome/locale/fr-FR/"],
  ["locale", "xnote", "gl", "chrome/locale/gl/"],
  ["locale", "xnote", "it-IT", "chrome/locale/it-IT/"],
  ["locale", "xnote", "ja-JP", "chrome/locale/ja-JP/"],
  ["locale", "xnote", "nl-NL", "chrome/locale/nl-NL/"],
  ["locale", "xnote", "pl-PL", "chrome/locale/pl-PL/"],
  ["locale", "xnote", "pt-BR", "chrome/locale/pt-BR/"],
]);

// This is the current "migration" version for preferences. You can increase it later
// if you happen to need to do more pref (or maybe other migrations) only once
// for a user.
const kCurrentLegacyMigration = 5;

// This is the list of defaults for the legacy preferences.
const kPrefDefaults = {
  usetag: true,
  dateformat: "yyyy-mm-dd - HH:MM",
  width: 250,
  height: 200,
  horPos: 250,
  vertPos: 250,
  show_on_select: true,
  show_in_messageDisplay: true,
  show_first_x_chars_in_col: 20,
  storage_path: "[ProfD]XNote",
  show_full_in_messageDisplay: false,
  fetched_old_XNotes: false,
  show_column: true,
};

async function migratePrefs() {
  //console.debug("migratePrefs called.")
  const results = await browser.storage.local.get("preferences");

  const currentMigration =
    results.preferences && results.preferences.migratedLegacy ?
      results.preferences.migratedLegacy : 0;

  if (currentMigration >= kCurrentLegacyMigration) {
    return;
  }

  let prefs = results.preferences || {};

  if (currentMigration < 1) {
    for (const prefName of Object.getOwnPropertyNames(kPrefDefaults)) {
      let oldPrefName = prefName;
      switch (prefName) {
        case "horPos": {
          oldPrefName = "HorPos";
          break;
        }
        case "vertPos": {
          oldPrefName = "VertPos";
          break;
        }
      }
      prefs[prefName] = await browser.xnoteapi.getPref(oldPrefName);
      if (prefs[prefName] === undefined) {
        prefs[prefName] = kPrefDefaults[prefName];
      }
    }
  }

  if (currentMigration < 2) {
    prefs["show_in_messageDisplay"] = kPrefDefaults["show_in_messageDisplay"];
    setTbPref("extensions.xnote.show_in_messageDisplay", kPrefDefaults["show_in_messageDisplay"]);
  }

  if (currentMigration < 3) {
    prefs["show_full_in_messageDisplay"] = kPrefDefaults["show_full_in_messageDisplay"];
    setTbPref("extensions.xnote.show_full_in_messageDisplay", kPrefDefaults["show_full_in_messageDisplay"]);
  }

  if (currentMigration < 4) {
    //this should logically be done here, but it must wait for main() to set prefs in the xnoteapi
    //  await getOldXNotes();
    prefs["fetched_old_XNotes"] = kPrefDefaults["fetched_old_XNotes"];
    setTbPref("extensions.xnote.fetched_old_XNotes", kPrefDefaults["fetched_old_XNotes"]);
  }

  if (currentMigration < 5) {
    //this should logically be done here, but it must wait for main() to set prefs in the xnoteapi
    //  await getOldXNotes();
    prefs["show_column"] = kPrefDefaults["show_column"];
    setTbPref("extensions.xnote.show_column", kPrefDefaults["show_column"]);
  }

  prefs.migratedLegacy = kCurrentLegacyMigration;
  //console.debug("Storing migrated preferences.");
  await browser.storage.local.set({ "preferences": prefs });
}

async function getTbPref(name) {
  return browser.xnoteapi.getTbPref(name);
}

async function setTbPref(name, value) {
  browser.xnoteapi.setTbPref(name, value);
}

function getPreferences() {
  // Why would you want to work with the cached values?
  return preferenceCache;
}

async function setPreferences(preferences) {
  preferenceCache = preferences;
  browser.storage.local.set({ preferences });
  browser.xnoteapi.setPreferences(preferences);
}

async function selectDirectory(startDir, title) {
  let result = await browser.xnotefiles.selectDirectory(null, startDir, title);
  //console.debug("select directory returns: " + result);
  return result;
}

async function getProfileDirectory() {
  return await browser.xnotefiles.getProfileDirectory();
}

async function appendRelativePath(basePath, extension) {
  return await browser.xnotefiles.appendRelativePath(basePath, extension);
}

async function wait(t) {
  //	let t = 5000;
  await new Promise(resolve => window.setTimeout(resolve, t));
}

// landing windows.
messenger.runtime.onInstalled.addListener(async ({ reason, temporary }) => {
  // if (temporary) return; // skip during development
  //  console.log("install reason:", reason);
  // setTimeout( () => { let url = messenger.runtime.getURL("/popup/xnotePopUp/xnote-window.html"); messenger.windows.create({ url:url, type:"panel", width: 210 });}, 200);
  setTimeout(() => { let url = messenger.runtime.getURL("/popup/donations.html"); messenger.windows.create({ url: url, type: "popup" }); }, 1000);
  donTimer = setInterval(() => { let url = messenger.runtime.getURL("popup/donations1.html"); messenger.windows.create({ url: url, type: "popup" }); }, 3 * 24 * 60 * 60 * 1000);

  switch (reason) {
    case "install":
      {
        let url = browser.runtime.getURL("popup/installed.html");
        //await browser.tabs.create({ url });
        await messenger.tabs.create({ url });
        //       let wID1 = await browser.windows.create({ url, type: "popup", width: 910, height: 750, });
        //       console.log ("wid", wID1);   
      }
      break;
    // see below
    case "update":
      {
        let url = browser.runtime.getURL("popup/update.html");
        //        let url2 = browser.runtime.getURL("popup/installed.html");
        await browser.tabs.create({ url });
        //   let wID = await browser.windows.create({ url, type: "popup", width: 910, height: 750, });
        //        console.log ("wid", wID);
        //       debugger;
        //        let tID = await messenger.tabs.create({active:true, index:1, url: "http://www.google.com", windowId: wID.id});
        ////   let tID = await messenger.tabs.create({windowId: wID.id});
        //    tID = await messenger.tabs.create({windowId: wID.id});
        //     tID = await messenger.tabs.create({windowId: wID.id});
        //      console.log ("tid", tID);   
      }
      break;
    // see below
  }
});



async function removeNoteOnFolderChange(tab, displayedFolder) {
  let actMailTab = await messenger.mailTabs.getCurrent();
  if (actMailTab && (tab.id == actMailTab.id)) {
    //    console.log("Folchange", tab, displayedFolder);
    messenger.xnoteapi.closeNoteWindow(tab.windowId);
    xnote.text = "";
  };

};





messenger.NotifyTools.onNotifyBackground.addListener(async (info) => {
  switch (info.command) {



    case "closeNote":
      let actMailTab = await messenger.mailTabs.getCurrent();
      if (true) {
        //    console.log("Folchange", tab, displayedFolder);
        messenger.xnoteapi.closeNoteWindow(actMailTab.windowId);
        xnote.text = "";
      };
      break;
    case "searchXNotes":
      //    console.log("xn bgr searh");
      let answ;
      try {
        answ = await messenger.runtime.sendMessage("expressionsearch@opto.one", { command: "searchXNotes" });
        messenger.xnoteapi.closeNoteWindow(activeInfo.windowId);

      }
      catch (e) {
        let url = messenger.runtime.getURL("popup/ExprSearch.html"); messenger.windows.create({ url: url, type: "popup" });
        //      console.log("ans exp", answ);
      };
      break;
    case "setBookmark":
      messenger.runtime.sendMessage("bookmarks@opto.one", { content: "addXnoteBookmark" }, {});
      break;
    case "saveXNote":
      let cc = { [info.hdrMsgId]: info.XNote };
      //    console.log("toDB", cc);
      if (!xnoteKeys.includes(info.hdrMsgId)) xnoteKeys.push(info.hdrMsgId);

      let msg = { command: "saveXnote", hdrMsgId: info.hdrMsgId, note: info.XNote, xnoteKeys: xnoteKeys };
      await messenger.storage.local.set(cc);
      if (!xnoteKeys.includes(info.hdrMsgId)) xnoteKeys.push(info.hdrMsgId);
      await messenger.storage.local.set({ "keys": xnoteKeys });
      /*
      //coming later
     */
      //115     let resp = await browser.runtime.sendMessage(
      //       "expressionsearch@opto.one",
      //       msg
      //     );

      break;

    case "deleteXNote":
      //console.log("delDB", info.hdrMsgId);
      await messenger.storage.local.remove(info.hdrMsgId.toString());
      xnoteKeys = xnoteKeys.filter(item => item !== info.hdrMsgId.toString());
      await messenger.storage.local.set({ "keys": xnoteKeys });
      xnote.text = "";
      //    xnote.date = xnote1.date;
      await messenger.storage.local.set({ "xnote": xnote }); //to forward to messagedisplayscript

      /*115
           let msgD = { command: "deleteXnote", xnoteKeys: xnoteKeys, hdrMsgId: info.hdrMsgId };
           let respD = await browser.runtime.sendMessage(
             "expressionsearch@opto.one",
             msgD
           );
     */
      break;

    case "getXNote":
      let note = await messenger.storage.local.get(info.hdrMsgId);
      //          console.log("note", note);
      break;

    case "getCommandKey":
      let commands = await messenger.commands.getAll();//.local.get(info.hdrMsgId);
      //   console.log("comm", commands[0]);
      //          console.log("note", note);
      // let comm1 = commands.filter(  (comm) => {   (comm.name != "openXNoteWindow")   });
      return commands[0];
      break;
      break;

    case "updateTag":
      //       console.log("updateTag", info);
      //      let mailTabsArr = await messenger.mailTabs.query({active:true, lastFocusedWindow:true});  //active:true,
      //      console.log("mailtabs", mailTabsArr);
      //let msg1 = await browser.messageDisplay.getDisplayedMessage(tab.id)

      let msg1 = await messenger.messages.get(info.info.msgId);
      //   let msgs = [msg];//await messenger.mailTabs.getSelectedMessages(mailTabsArr[0].id);
      //   console.log("tags",  msgs, msgs.messages[0]);
      let tags = msg1.tags;//msgs.messages[0].tags;
      //       console.log("tags", tags, msg1);// msgs);
      let hasXNoteTag = false;
      if (tags.length) hasXNoteTag = tags.toString().includes("xnote");
      if (info.info.addTag && !hasXNoteTag) {
        tags.push("xnote");
        //      messenger.messages.update(msgs.messages[0].id,{tags: tags});
        //    console.log("tags", tags,  tags.toString(), hasXNoteTag);

        messenger.messages.update(info.info.msgId, { tags: tags });  //
        //        messenger.messages.update(msg1.id, { tags: tags });  //

      } else {
        if (info.info.removeTag) {  //&&hasXNoteTag
          let index = tags.indexOf("xnote");
          if (index > -1) tags.splice(index, 1);
          if (tags.length == 0) tags.push("");
          messenger.messages.update(info.info.msgId, { tags: tags });  //
          //          messenger.messages.update(msg1.id, { tags: tags });  //
        };

      };
      //  let message = await browser.messageDisplay.getDisplayedMessage(tab.id);
      //     let commands1 = await messenger.commands.getAll();//.local.get(info.hdrMsgId);
      //   console.log("comm", commands[0]);
      //          console.log("note", note);
      // let comm1 = commands.filter(  (comm) => {   (comm.name != "openXNoteWindow")   });
      return;
  }



});

async function addToDB(msgKey) {
  let note = await browser.xnoteapi.getXNoteFromMsgKey(msgKey);
  if (!note) return;
  let cc = { [msgKey]: note };
  //console.log("note", cc);
  await messenger.storage.local.set(cc);
  if (!xnoteKeys.includes(msgKey)) {
    xnoteKeys.push(msgKey);
    await messenger.storage.local.set({ "keys": xnoteKeys });
  };

};

async function getOldXNotesOneByOne() {
  //this should logically be done in migraterefs, but it must wait for main() to set prefs in the xnoteapi

  await new Promise(resolve => setTimeout(resolve, 1000));
  //  let notesk = await browser.xnoteapi.getAllXNotesFromDirectory();
  // debugger;
  let notesk = await browser.xnoteapi.getAllXNoteKeys();
  // console.log("fetch old xnotes", notesk.keys.length, notesk.keys, typeof notesk.keys, Array.isArray(notesk.keys));
  for (let i = 0; i < notesk.keys.length; i++)  addToDB(notesk.keys[i]);
  /**/

  //set pref
  preferenceCache["fetched_old_XNotes"] = true;
  let prefs = preferenceCache;
  setPreferences(prefs);

};



async function getOldXNotes() {
  //this should logically be done in migraterefs, but it must wait for main() to set prefs in the xnoteapi

  await new Promise(resolve => setTimeout(resolve, 1000));
  let notesk = await browser.xnoteapi.getAllXNotesFromDirectory();
  //  console.log("fetch old xnotes", notesk);
  /**/
  notesk.all.forEach(async element => {

    let cc = { [element[0]]: element[1] };
    await messenger.storage.local.set(cc);
    if (!xnoteKeys.includes(element[0])) {
      xnoteKeys.push(element[0]);
      await messenger.storage.local.set({ "keys": xnoteKeys });
    };
  });
  //set pref
  preferenceCache["fetched_old_XNotes"] = true;
  let prefs = preferenceCache;
  setPreferences(prefs);

};


function handleMessage(request, sender, sendResponse) {
  sendResponse({ response: "Response from background script" });
  //console.log(`A content script sent a message: ${request.command}`);
  if (request.command == "stopDonPopup") clearInterval(donTimer);
}

browser.runtime.onMessage.addListener(handleMessage);

// This is the final onActivated function, which is called 
// * on the active tab after load
// * on tabs, which have been created as active
// * on each standard tab switch
async function onSafeActivated(activeInfo) {
  //console.log("onSafeActivated");
  await new Promise(resolve => setTimeout(resolve, 1000));
  let tab = await messenger.tabs.get(activeInfo.tabId);
  //  console.log(`Tab ${tab.id} in window ${tab.windowId} was activated (mailtab: ${tab.mailTab}).`);
  //  messenger.Utilities.isMailTab(tab.mailTab || tab.type == "messageDisplay", tab.type == "messageDisplay");
}

// Wait until we have at least one window with an active tab.
async function waitForWindow() {
  let windows = await browser.windows.getAll({ windowTypes: ["normal"] });
  for (let win of windows) {
    let activeTabs = await messenger.tabs.query({ active: true, windowId: win.windowId });
    if (activeTabs.length == 1) {
      onSafeActivated({ tabId: activeTabs[0].id, windowId: activeTabs[0].windowId });
    } else {
      console.error(`Did not find exactly one active tab in window ${win.windowId}:`, activeTabs);
    }
  }
}



//var portFromBookmarks = null;
async function main() {
  await waitForWindow();
  await new Promise(resolve => setTimeout(resolve, 1000));
  //console.log("before WL in main");

  messenger.WindowListener.registerWindow(
    "chrome://messenger/content/messenger.xhtml",
    //   "about:3pane",  
    "chrome/content/scripts/xn-xnote-overlay.js"
  );

  messenger.WindowListener.registerWindow(
    //  "chrome://messenger/content/messenger.xhtml",
    "about:3pane",
    "chrome/content/scripts/xn-xnote-aboutOverlay.js"
  );

  messenger.WindowListener.startListening();

  await migratePrefs();


  preferenceCache = (await browser.storage.local.get("preferences")).preferences;
  await browser.xnoteapi.setPreferences(preferenceCache);
  await browser.xnoteapi.init();

  //if (preferenceCache["fetched_old_XNotes"] == false) getOldXNotes();
  getOldXNotesOneByOne();


  messenger.tabs.onActivated.addListener(async (activeInfo) => {
    let lastTabId = activeInfo.previousTabId;
    // console.log("tab actic, last, new", activeInfo.previousTabId, activeInfo.tabId); //102tabId;
    lastWindow = activeInfo.windowId;
    let tabInfo = await messenger.tabs.get(activeInfo.tabId);
    if (!tabInfo.mailTab) {
      if (lastTabId) {
        tabInfo = await messenger.tabs.get(lastTabId);
        //    console.log("tabaciv, close xnote", tabInfo);
        messenger.xnoteapi.closeNoteWindow(tabInfo.windowId);
        //102     messenger.xnoteapi.closeNoteWindow(activeInfo.windowId);
        xnote.text = "";
      };
      return;
    }
    else {
 try {     let xnote_displayed = await messenger.xnoteapi.hasOpenNoteWindow(activeInfo.windowId);
      if (!xnote_displayed) {

        let message = await browser.messageDisplay.getDisplayedMessage(activeInfo.tabId)
        let xnote1 = await messenger.xnoteapi.getXNote(message.id);
        if (xnote1.text != "") await messenger.xnoteapi.openNoteWindow(activeInfo.windowId, message.id, true, true);
      }
    }
    catch (e) {};
    };

    if (tabInfo.type != "mail") {
      messenger.xnoteapi.closeNoteWindow(activeInfo.windowId);
      xnote.text = "";
    };

  });


  messenger.tabs.onCreated.addListener((tab) => {
    //    console.log("created tabname", tab.title);
    if (!tab.mailTab) {
      messenger.xnoteapi.closeNoteWindow(tab.windowId);
      xnote.text = "";
    };
  });


  messenger.mailTabs.onDisplayedFolderChanged.addListener(removeNoteOnFolderChange);
  messenger.mailTabs.onSelectedMessagesChanged.addListener(removeNoteOnFolderChange);

  browser.browserAction.onClicked.addListener(async (tab, info) => {
    //   messenger.xnoteapi.getAllXNoteKeys();
    let xnote_displayed = await messenger.xnoteapi.hasOpenNoteWindow(tab.windowId);
    if (!xnote_displayed) {
      let message = await browser.messageDisplay.getDisplayedMessage(tab.id)
      await messenger.xnoteapi.openNoteWindow(tab.windowId, message.id, true, true);
    } else {
      messenger.xnoteapi.closeNoteWindow(tab.windowId);
    }
  });




  messenger.messageDisplay.onMessageDisplayed.addListener(async (tab, message) => {
    xnote.currentDisplayedMessage = message.id;
    //debugger;
    let xnote1 = await messenger.xnoteapi.getXNote(message.id);
    if (preferenceCache["show_in_messageDisplay"] == false) xnote1.text = "";
    xnote1.show_full_in_messageDisplay = preferenceCache["show_full_in_messageDisplay"];
    xnote.text = xnote1.text;
    xnote.date = xnote1.date;
    await messenger.storage.local.set({ "xnote": xnote });

    await messenger.xnoteapi.closeNoteWindow(tab.windowId);
    if (xnote1.text != "") await messenger.xnoteapi.openNoteWindow(tab.windowId, message.id, false, true);
    //  console.log("onMessDispl messid",  message.headerMessageId);
    /*   let note = (await messenger.storage.local.get(message.headerMessageId))[message.headerMessageId];
  //    console.log("note", note);
   if (   xnote.window )  {
  //  console.log("winid", xnote.window);
    messenger.windows.remove(xnote.window.id);
    //   xnote.window.close();
       xnote.window = "null";
   };
  
      let url = messenger.runtime.getURL("/popup/xnotePopUp/xnote-window.html"); 
      if ( Object.keys(note).length > 0  ) xnote.window = await  messenger.windows.create({ url:url, type:"panel", width: 210 });
     // if ( note ) xnote.window = await  messenger.windows.create({ url:url, type:"panel", width: 210 });
   //   console.log("winid", message, xnote.window, message.id, message.headerMessageId, note, Object.keys(note), Object.keys(note).length);
  */

  });





  /**/
  messenger.runtime.onMessageExternal.addListener(async (message, sender, sendResponse) => {
    if (sender.id === "expressionsearch@opto.one") {
      if (message.command == "getXnote") {
        //         console.log("hdr", message.hdrID);
        let note = await messenger.storage.local.get(message.hdrID);
        //         console.log("note", note);
        return note;

      };

      if (message.command == "getXnoteDir") {
        //console.log("prefs", getPreferences().storage_path);

        //         console.log("note", note);
        //sendResponse({resp:"test3"});//note);
        return getPreferences().storage_path;

      };


      if (message.command == "getAllXNotes") {
        //       let notes = await messenger.storage.local.get(null);
        let keys = await messenger.storage.local.get("keys");
        //     console.log("keys", keys);
        let all = [];
        // let one = {};
        for (let i = 0; i < keys.keys.length; i++) {
          let key = keys.keys[i];
          let elem = await messenger.storage.local.get(key);
          //         console.log("key", key, "elem", elem);
          //         console.log("elkey", elem[key]);
          all[key] = elem[key];
        };
        //       console.log("all", notes, "onebyone", all );
        return all;

      };



    };





  });




  /*
   * Show note info in message display
   */

  async function toMessDisplScript(message) {
    if (message.command == "getXNote") {
      //115     let msg = await messenger.messageDisplay.getDisplayedMessage(sender.tab.id);
      let msg = await messenger.messages.get(xnote.currentDisplayedMessage);
      //   console.log("sender", sender,msg);
      let xnote1 = await messenger.xnoteapi.getXNote(msg.id);
      if (preferenceCache["show_in_messageDisplay"] == false) xnote1.text = "";
      xnote1.show_full_in_messageDisplay = preferenceCache["show_full_in_messageDisplay"];
      // sendResponse({ xnote: xnote1 });
      return { xnote: xnote1 };
    };

  };

  var doHandleCommand = async (message, sender) => {
    const { command } = message;
    const {
      tab: { id: tabId },
    } = sender;

    const messageHeader = await browser.messageDisplay.getDisplayedMessage(tabId);

    // console.log("messageHeader", messageHeader);

    // Check for known commands.
    switch (command.toLocaleLowerCase()) {
      case "getnotificationdetails":
        {
          // Create the information chunk we want to return to our message content
          // script.
          //        console.log("getnotificationdetails");
          return {
            text: `Mail subject is "${messageHeader.subject}"`,
          };
        }
        break;

      case "markunread":
        {
          // Get the current message from the given tab.
          if (messageHeader) {
            // Mark the message as unread.
            browser.messages.update(messageHeader.id, {
              read: false,
            });
          }
        }
        break;
    }
  };

  browser.runtime.onMessage.addListener((message, sender, sendResponse) => {
    //   console.log(message, sender, xnote.currentDisplayedMessage);
    //    debugger;
    /*
        if (message.command == "getXNote") {
     //115     let msg = await messenger.messageDisplay.getDisplayedMessage(sender.tab.id);
     let msg = await messenger.messages.get(xnote.currentDisplayedMessage);
  //   console.log("sender", sender,msg);
          let xnote1 = await messenger.xnoteapi.getXNote(msg.id);
          if (preferenceCache["show_in_messageDisplay"] == false) xnote1.text = "";
          xnote1.show_full_in_messageDisplay = preferenceCache["show_full_in_messageDisplay"];
          sendResponse({ xnote: xnote1 });
        //  return xnote1;
        };
    
    let mmm = toMessDisplScript(message);
    //sendResponse({ xnote: "test" });
    //return { xnote: "test" };
    return toMessDisplScript(message);
    //return Promise.resolve({ response: mmm });
    */

    if (message && message.hasOwnProperty("command")) {
      // If we have a command, return a promise from the command handler.

      return doHandleCommand(message, sender);
    }
    return false;


  });

  // Only loads into new messages, so on install, it will not load into the
  // already open message.
  await messenger.messageDisplayScripts.register({
    js: [{ file: "mDisplay.js" }]
    //,
    //css: [{ file: "/src/message-content-styles.css" }],
  });


  messenger.commands.onCommand.addListener(async function (command, tab) {
    //console.log("command", command);
    if (command === "openXNoteWindow") {
      let xnote_displayed = await messenger.xnoteapi.hasOpenNoteWindow(tab.windowId);
      //console.log("commandx", xnote_displayed);
      if (!xnote_displayed) {
        let message = await browser.messageDisplay.getDisplayedMessage(tab.id)
        //       console.log("commandm", message);
        await messenger.xnoteapi.openNoteWindow(tab.windowId, message.id, true, true);
      } else {
        messenger.xnoteapi.closeNoteWindow(tab.windowId);
      }
    };
    if (command === "getOldXNotes") {
      getOldXNotes();
      //    console.log("getOldXNotes");
      /*      let xnote_displayed = await messenger.xnoteapi.hasOpenNoteWindow(tab.windowId);
            //        console.log("commandx", xnote_displayed);
            if (!xnote_displayed) {
              let message = await browser.messageDisplay.getDisplayedMessage(tab.id)
              //       console.log("commandm", message);
              await messenger.xnoteapi.openNoteWindow(tab.windowId, message.id, true, true);
            } else {
              messenger.xnoteapi.closeNoteWindow(tab.windowId);
            } */
    };
  });


  /*
  //leaving in, but seems double
    messenger.messageDisplay.onMessageDisplayed.addListener(async (tab, message) => {
      await messenger.xnoteapi.closeNoteWindow(tab.windowId);
      await messenger.xnoteapi.openNoteWindow(tab.windowId, message.id, false);
    });
  */
  /*
   * Start listening for opened windows. Whenever a window is opened, the registered
   * JS file is loaded. To prevent namespace collisions, the files are loaded into
   * an object inside the global window. The name of that object can be specified via
   * the parameter of startListening(). This object also contains an extension member.
   */

  // WE USE THIS ONLY FOR THE MENU ENTRIES - GET RID OF IT - USE MENUS API
  // messenger.WindowListener.registerWindow(
  //115 "chrome://messenger/content/messenger.xhtml",
  // "about:3pane",  "chrome/content/scripts/xn-xnote-overlay.js"
  // );
  // messenger.WindowListener.startListening();
}

main().catch(console.error);
